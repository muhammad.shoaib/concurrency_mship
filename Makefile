SERVER_PORT=8000
ifneq (,$(wildcard ./.env))
	include .env
	export
endif

run:
	python manage.py runserver ${SERVER_PORT}

run-gunicorn:
	gunicorn concurrency_mship.wsgi

run-db:
	@docker-compose up -d --build db

migrate:
	python manage.py migrate

qcluster:
	python manage.py qcluster

run-dramatiq-workers: ## Start the dramatiq worker jobs for the sennder tenant
	@set -a && . ./.env && set +a && \
	    export DJANGO_SETTINGS_MODULE="concurrency_mship.settings" && \
	    bash mothership/async_queue/run_dramatiq_workers.sh \
	    -f demo_app/module_tasks.txt
	    --watch .