import os
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-=a=eqtin8g@wj&ywcm9ubk&^llpcbac(5s0iasvt3a6685v49u'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # apps
    'demo_app',

    # other apps
    'django_q',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'concurrency_mship.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates']
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'concurrency_mship.wsgi.application'


# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': BASE_DIR / 'db.sqlite3',
#     }
# }

DEFAULT_DATABASE_CONNECTION_NAME = 'default'

DATABASES = {
    DEFAULT_DATABASE_CONNECTION_NAME: {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": os.getenv("DB_NAME"),
        "USER": os.getenv("DB_USER"),
        "PASSWORD": os.getenv("DB_PASSWORD"),
        "HOST": os.getenv("DB_HOST"),
        "PORT": os.getenv("DB_PORT"),
        "ATOMIC_REQUESTS": True,
        "OPTIONS": {  # global timeout 30s
            "options": "-c statement_timeout=30000"
        }
    }
}


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_TZ = True
STATIC_URL = 'static/'
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
BACKGROUND_JOB_TIMEOUT_SECONDS = 30 * 60

# Q_CLUSTER = {
#     'name': 'django_q_django',
#     'workers': 8,
#     'recycle': 500,
#     'timeout': 60,
#     'compress': True,
#     'save_limit': 250,
#     'queue_limit': 500,
#     'cpu_affinity': 1,
#     'label': 'Django Q',
#     'redis': {
#         'host': 'ec2-52-18-11-1.eu-west-1.compute.amazonaws.com',
#         'port': 9059,
#         'password': 'p948710311f252a334c3b21cabe0bd63f943f68f0824cd41932781e7793c785bf',
#         'db': 0, }
# }

Q_CLUSTER = {
    "name": 'concurrency_mship',
    "orm": DEFAULT_DATABASE_CONNECTION_NAME,
    "workers": 1,
    "label": "Django Q",
    # don't catch up with missed scheduled tasks
    "catch_up": False,
    # tasks pulled at once (max supported by SQS is 10)
    "bulk": 10,
    #
    # Not storing any successful tasks, since the curtailment
    # option of this setting is broken and tables grow very fast
    "save_limit": -1,
    #
    # max time a worker can work on a task (seconds)
    "timeout": BACKGROUND_JOB_TIMEOUT_SECONDS,
    #
    # Time before a non-acknowledged message is retried, should be higher than
    # timeout
    "retry": BACKGROUND_JOB_TIMEOUT_SECONDS + 5 * 60,
    #
    # Acknowledge messages when the task failed, preventing retries.
    # This is commented out because setting max-receives in our
    # SQS redrive policy should achieve the same while creating more
    # visibility into the number of failed tasks.
    "ack_failures": True,

    # redis
    # 'redis': {
    #     'host': 'localhost',
    #     'port': 6380,
    #     'db': 0,
    #     'password': None,
    #     'socket_timeout': None,
    #     'charset': 'utf-8',
    #     'errors': 'strict',
    #     'unix_socket_path': None
    # }
}

DRAMATIQ_REDIS_URI = os.getenv("DRAMATIQ_REDIS_URI")
REDIS_KEY_PREFIX = os.getenv("DRAMATIQ_REDIS_KEY_PREFIX")

# Strategy for launching asynchronous tasks (useful for unit tests)
STRATEGY_ASYNC = "ASYNC"
STRATEGY_SYNC = "SYNC"
STRATEGY_DO_NOT_RUN = "DO_NOT_RUN"
TASK_RUN_STRATEGY = os.getenv("TASK_RUN_STRATEGY", STRATEGY_ASYNC)
