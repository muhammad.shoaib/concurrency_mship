from contextlib import contextmanager

from django.db import DEFAULT_DB_ALIAS, connections


# Adopted from Django 3: https://github.com/django/django/pull/12944/files#


@contextmanager
def run_dramatiq_jobs_on_context_exit(*, using=DEFAULT_DB_ALIAS):
    """Context manager to capture transaction.on_commit() callbacks."""
    callbacks = []
    start_count = len(connections[using].run_on_commit)
    try:
        yield callbacks
    finally:
        # When RUN_WORKERS_SYNCHRONOUSLY is set to true, a dramatiq task
        # may schedule another dramatiq task, so we need to keep
        # calling the callbacks until none are being created any more
        while start_count != len(connections[using].run_on_commit):
            callbacks = [
                func
                for _, func in connections[using].run_on_commit[start_count:]
                if func.__name__
                in [
                    "run_dramatiq_actor_synchronously",
                    "schedule_dramatiq_actor",
                ]
            ]
            start_count = len(connections[using].run_on_commit)
            for callback in callbacks:
                callback()
