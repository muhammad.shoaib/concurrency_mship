from unittest.mock import MagicMock, patch

import pytest
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import connections, transaction
from django.db.utils import DEFAULT_DB_ALIAS, load_backend
from django.test import TestCase, override_settings

from sennder_utils.test import TransactionTestCase

from sennder.apps.ordering.tasks import get_and_assign_route_distance
from mothership.async_queue.decorators_for_testing import (
    run_dramatiq_jobs_on_context_exit,
)

from ..async_queue import run_async


"""
Helpers
"""


@run_async
def async_job(test_param: int):
    del test_param
    called_only_by_jobs()


@run_async(max_retries=3)
def async_job_with_retries(test_param: int):
    pass


def called_only_by_jobs():
    pass


def _create_new_db_connection():
    """
    Creates a new connection to the DB currently in use
    """
    db = connections.databases[DEFAULT_DB_ALIAS]
    backend = load_backend(db["ENGINE"])
    return backend.DatabaseWrapper(db, DEFAULT_DB_ALIAS)


"""
Tests
"""


class TestDramatiqWrapper(TestCase):
    @patch("sennder.async_queue.tests.test_async_queue.called_only_by_jobs")
    def test_send_task_with_decorator(self, mock: MagicMock):
        with run_dramatiq_jobs_on_context_exit():
            with transaction.atomic():
                async_job.send(10)
                # Check that the function is not called before the end of the transaction
                self.assertEqual(mock.called, False)

        self.assertEqual(mock.called, True)

    @patch("sennder.async_queue.tests.test_async_queue.called_only_by_jobs")
    def test_send_with_options_and_decorator(self, mock: MagicMock):
        with run_dramatiq_jobs_on_context_exit():
            with transaction.atomic():
                async_job.send_with_options(args=(10,))
                # Check that the function is not called before the end of the transaction
                self.assertEqual(mock.called, False)

        self.assertEqual(mock.called, True)

    @override_settings(TASK_RUN_STRATEGY=settings.STRATEGY_DO_NOT_RUN)
    @patch("sennder.async_queue.tests.test_async_queue.called_only_by_jobs")
    def test_async_task_ignored(self, mock: MagicMock):
        with run_dramatiq_jobs_on_context_exit():
            async_job.send_with_options(args=(10,))

        self.assertEqual(mock.called, False)


class TestAsyncQueuesDBRecovery(TransactionTestCase):
    def setUp(self):
        super()

        # Save the original test DB connection
        self.vanilla_connection = connections["default"]

        # Create a temporary new DB connection that we can mess around with ...
        self.temp_connection = _create_new_db_connection()
        # ... and set it (temporarily) as the default do be used
        connections["default"] = self.temp_connection

    def tearDown(self):
        # Close the temporary DB connection we've messed around with
        self.temp_connection.close()

        # Restore the original test DB connection as default
        connections["default"] = self.vanilla_connection

        super()

    def test_actors_can_recover_from_aborted_db_connections(self):
        with self.vanilla_connection.cursor() as vanilla_cursor:
            with self.temp_connection.cursor() as temp_cursor:
                # Find out our temp connection's process ID ...
                temp_cursor.execute(f"SELECT pg_backend_pid();")
                pid = temp_cursor.fetchone()[0]

                # ... and kill it from the DB on the DB side
                # (using the original connection,
                # so that our code doesn't know about it yet)
                vanilla_cursor.execute(f"SELECT pg_terminate_backend({pid});")

        with run_dramatiq_jobs_on_context_exit():
            # Run an arbitrary asnyc task that requires a DB connection
            with pytest.raises(ObjectDoesNotExist):
                get_and_assign_route_distance.send(42)

    def test_actors_can_recover_from_closed_db_connections(self):
        self.temp_connection.connect()
        self.temp_connection.connection.close()

        with run_dramatiq_jobs_on_context_exit():
            # Run an arbitrary asnyc task that reqires a DB connection
            with pytest.raises(ObjectDoesNotExist):
                get_and_assign_route_distance.send(42)


def test_default_actor_options():
    assert async_job.actor.options == {"max_retries": 0}


def test_retry_option():
    assert async_job_with_retries.actor.options == {"max_retries": 3}
