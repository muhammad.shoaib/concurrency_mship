#!/bin/bash
set -eu

FILES=()
PARAMS=""
while (( "$#" )); do
  case "$1" in
    -f)
      FILES+=("$2")
      shift
      shift
      ;;
    *)
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

MODULES=""
for item in ${FILES[*]}
do
  MODULES="$MODULES $(cut -d$'\n' -f1- $item)"
done

ARGUMENTS="$MODULES $PARAMS"
echo "$ARGUMENTS"

dramatiq mothership.async_queue.brokers:broker mothership.async_queue.django_setup $ARGUMENTS
