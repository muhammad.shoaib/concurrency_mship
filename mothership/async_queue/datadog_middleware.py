import logging
import random
from time import time
from typing import List

import redis
from django.conf import settings
from dramatiq.middleware import Middleware

from sennder.utils.datadog import get_datadog_env, get_statsd_or_mock


log = logging.getLogger(__name__)


def current_millis():
    """Returns the current UNIX time in milliseconds."""
    return int(time() * 1000)


class DatadogMiddleware(Middleware):
    """A middleware that exports stats to Datadog via DogStatsD."""

    def __init__(self):
        self.delayed_messages = set()
        self.message_start_times = {}
        self.statsd = None

    def _build_tags(self, message) -> List[str]:
        return [
            f"queue:{message.queue_name}",
            f"actor:{message.actor_name}",
            f"environment:{get_datadog_env()}",
        ]

    def initialize_datadog_statsd(self):
        if self.statsd is None:
            self.statsd = get_statsd_or_mock()

    def _update_queue_num_messages_gauge(self, queue_name, broker, tags):
        queue_redis_key = f"{settings.REDIS_KEY_PREFIX}:{queue_name}"
        try:
            queue_length = broker.client.llen(
                bytes(queue_redis_key, encoding="utf8")
            )
        except redis.exceptions.ResponseError:
            log.warning(
                f"Unable to find {queue_name} in redis. "
                "Possibly does not yet exist"
            )
        else:
            log.info(f"Dramatiq-queue {queue_name} has {queue_length} messages")
            self.statsd.gauge(
                metric=f"dramatiq.num_messages_in_queue.{queue_name}",
                value=queue_length,
                tags=tags,
            )

    def before_declare_actor(self, broker, actor):
        self.initialize_datadog_statsd()

    def after_process_boot(self, broker):
        self.initialize_datadog_statsd()

    def after_nack(self, broker, message):
        tags = self._build_tags(message)
        self.statsd.increment(
            metric="dramatiq.total_rejected_messages", tags=tags
        )

    def after_enqueue(self, broker, message, delay):
        tags = self._build_tags(message=message)
        if "retries" in message.options:
            self.statsd.increment(
                metric="dramatiq.total_retried_messages", tags=tags
            )
        if random.randint(0, 19) == 0:
            # Obtaining and sending this metric has some overhead.
            # Thus we only send it randomly with a 1:20 chance.
            self._update_queue_num_messages_gauge(
                queue_name=message.queue_name, broker=broker, tags=tags
            )

    def before_delay_message(self, broker, message):
        self.delayed_messages.add(message.message_id)
        name_of_delay_queue = f"{message.queue_name}.DQ"
        self._update_queue_num_messages_gauge(
            queue_name=name_of_delay_queue,
            broker=broker,
            tags=self._build_tags(message=message),
        )

    def before_process_message(self, broker, message):
        tags = self._build_tags(message=message)
        if message.message_id in self.delayed_messages:
            self.delayed_messages.remove(message.message_id)
        self.statsd.increment(metric="dramatiq.in_progress_messages", tags=tags)
        self.message_start_times[message.message_id] = current_millis()

    def after_process_message(
        self, broker, message, *, result=None, exception=None
    ):
        tags = self._build_tags(message)
        message_start_time = self.message_start_times.pop(
            message.message_id, current_millis()
        )
        message_duration = current_millis() - message_start_time
        self.statsd.histogram(
            metric="dramatiq.message_durations",
            value=message_duration,
            tags=tags,
        )
        self.statsd.decrement(metric="dramatiq.in_progress_messages", tags=tags)
        self.statsd.increment(metric="dramatiq.total_messages", tags=tags)
        if exception is not None:
            self.statsd.increment(
                metric="dramatiq.total_errored_messages", tags=tags
            )

    after_skip_message = after_process_message
