from django.conf import settings
from dramatiq.brokers.redis import RedisBroker

# from .datadog_middleware import DatadogMiddleware


broker = RedisBroker(
    url=settings.DRAMATIQ_REDIS_URI, namespace=settings.REDIS_KEY_PREFIX
)
# broker.add_middleware(DatadogMiddleware())
