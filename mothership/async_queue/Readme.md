# Async queue

## Quickstart: Steps to add an async job:

1. Add the decorator `@run_async`

2. Schedule the job by calling `decorated_function.send(<json-serializable function arguments>)`

3. Create (or update if exist) module's `module_tasks.txt` with module path in that format `{path}.{to}.{the}.{module}`. Each module name should be on a new line.

4. Add a script to the worker definitions in `helm/values.yaml` to run your `dramatiq` worker on staging/prod environments

5. Add new `worker-dramatiq-{ module_name }-deployment.yaml` file to `helm/templates`. This is necessary to create a new isolated dramatiq worker process that will run your tasks in module. If module is already present please skip the step.

6. Add path to your's module `module_tasks.txt` to Makefile in format `-f path/to/the/module_tasks.txt`. If module path is already there please skip the step.


### Running the queue workers locally

To run the queue workers locally, you can use `make run-sennder-workers` or `make run-poste-workers`.
This will set local environment configuration for the respective tenant and then start the worker process.

## Documentation

This module supports a queue implementation with dramatiq for asynchronous background task execution.

### Usage

You can create a dramatiq task (called `actor` in dramatiq) by importing `run_async` from `async_queue.py`
and using it as a function decorator.

```
@run_async
def say_something(what_to_say: str) -> None:
    print(what_to_say)

```

To execute this asynchronously, call it like this:

```
say_something.send(what_to_say="Hello")
```

To execute this asynchronously after a delay, call it like this:
```
say_something.send_with_options(args=("Hello"), delay=10000) # Will be executed after 10000ms
```

It will then be sent as a job to the job queue and be picked up by a worker.
Every decorated function has its own queue.

Note that asynchronous tasks can only take primitive, json-serializable values as arguments.


### Implementation

This library creates a dedicated message queue for each function decorated with `@run_async`.

The message queues are stored in a redis cluster.
They are named after their fully qualified function name and prefixed with the
`REDIS_KEY_PREFIX` as defined in the settings.

For example, a queue might be called:
`sennder_api-sennder_apps_integrations_co3_pull_tasks_run_task`


### Monitoring

The current number of messages of any queue is reported as a gauge metric in datadog.

The metric is named as follows:
`dramatiq.num_messages_in_queue.<queue-name>`

For performance reasons, this metric is randomly sampled: Every message that is scheduled into the
queue has a 1/20 chance of triggering the reporting of the metric. This means that for queues with few messages,
this metric will be very rarely reported. A future improvement will be to run a regularly-scheduled
job that will ensure regular reporting of these metrics.


### Error handling

All logs are logged to datadog. Additionally errors and warnings are reported to sentry.

### Unit testing

In our wrapper implementation, dramatiq jobs are scheduled when the on_commit signal is called.
This is to avoid race conditions where the scheduling process has not yet committed changes to the DB
when the async job process is already querying this data from the DB.

However, unit tests inheriting from Django's `TestCase` will NOT call this signal.

**To ensure that dramatiq jobs are scheduled in unit tests, you must use the `runDramatiqJobsOnCommit` decorator as follows:

```python

from sennder.async_queue.decorators_for_testing import run_dramatiq_jobs_on_context_exit
class UnitTest:

    def test_something(self):
        ...
        with run_dramatiq_jobs_on_context_exit():
            ...
            call_code_that_submits_jobs()
            ...
        # The jobs are run when the context manager closes
        ...
```

In the unit tests, the `TASK_RUN_STATEGY` variable is set to `SYNC` by default.
This means that the tasks will be launched synchronously instead of going through a message broker.
If delayed tasks are started in the functions you are calling, you will most likely want them not to be started.
This could change behaviour if they were called synchronously instead of being planned

This is the way to proceed in your unit tests:
```python
@override_settings(TASK_RUN_STRATEGY=settings.STRATEGY_DO_NOT_RUN)
def test_something(self, mock: MagicMock):
    call_code_that_delay_jobs() # Asynchronous tasks will be ignored
```


### Limitations

* Scheduled jobs are not supported by dramatiq (to support scheduled jobs, we will need to enlist the help of another package such as `APScheduler`)

* Jobs are never re-tried in our current implementation (but available in dramatiq)

Since Dramatiq itself supports delayed jobs as well as re-tries, we can start to support these by updating the wrapper
code in this module. This will likely require only small modifications.
