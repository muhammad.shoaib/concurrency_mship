import json
import logging

from django.conf import settings
from django.db import connection, transaction
from django.db.utils import InterfaceError, OperationalError
from dramatiq import Actor as DramatiqActor
from dramatiq import actor as dramatiq_actor
from dramatiq import set_broker

from .brokers import broker


log = logging.getLogger(__name__)


set_broker(broker)


def fully_qualified_function_name(fn):
    return f"{fn.__module__}_{fn.__name__}".replace(".", "_")


class ActorWrapper:
    """Wrapper around dramatiq's Actor class to delay actor
    scheduling until after the db transaction has completed.
    We need to delay the message dispatch to the queue until the current
    transaction has been committed to avoid race conditions.
    """

    def __init__(self, actor: DramatiqActor):
        self.actor = actor

    def __call__(self, *args, **kwargs):
        return self.actor(*args, **kwargs)

    def send(self, *args, **kwargs):
        return self.send_with_options(args=args, kwargs=kwargs)

    def send_with_options(
        self, *, args=None, kwargs=None, delay=None, **options
    ):
        def run_dramatiq_actor_synchronously():
            """
            We directly call the function in unit tests, bypassing the queue
            Note that on_commit does not get called in unit tests.
            If you need the job to run in the unit test,
            use the .decorators_for_testing.execute_on_commit
            """
            try:
                json.dumps((args, kwargs))
            except TypeError as e:
                raise ValueError(
                    f"Arguments to async actors must be JSON-serializable!: {e}"
                )
            log.info(f"Running {self} job synchronously")
            return self.actor(*args or (), **kwargs or {})

        def schedule_dramatiq_actor():
            log.info(f"Scheduling {self} into {self.actor.queue_name}")
            return self.actor.send_with_options(
                args=args, kwargs=kwargs, delay=delay, **options
            )

        def ignore_dramatiq_task():
            log.info(f"{self} : job has been ignored")

        strategies = {
            settings.STRATEGY_ASYNC: schedule_dramatiq_actor,
            settings.STRATEGY_SYNC: run_dramatiq_actor_synchronously,
            settings.STRATEGY_DO_NOT_RUN: ignore_dramatiq_task,
        }

        if settings.TASK_RUN_STRATEGY not in strategies:
            raise ValueError(
                f"{settings.TASK_RUN_STRATEGY} is not a supported strategy\n"
                "Check that the variable TASK_RUN_STRATEGY is properly configured"
            )

        transaction.on_commit(strategies[settings.TASK_RUN_STRATEGY])


def run_async(fn=None, *args, **kwargs):
    # keeping previous behaviour where max_retries was hardcoded to 0
    max_retries = kwargs.pop("max_retries", 0)

    if "queue_name" in kwargs:
        raise RuntimeError(
            "Do not pass queue_name to the decorator,"
            "this argument is overwritten in the sennder wrapper for "
            "dramatiq."
        )

    def decorator(fn):
        def fn_with_db_check(*inner_args, **inner_kwargs):
            """
            Small wrapper to ensure a working DB connection
            for the wrapped method
            """
            try:
                # Quickly probe the DB - this helps us detect certain connection errors before starting the method
                probe_cursor = connection.cursor()
                # Some issues (such as the connection being closed from the DB side)
                # are only detected on an actual query
                probe_cursor.execute("SELECT 1;")
            except (InterfaceError, OperationalError):
                # If the probe fails, try to reconnect to the DB
                connection.connect()
            return fn(*inner_args, **inner_kwargs)

        # We set the queue name from the fully qualified function name.
        # This means that every function's job run in their own queue.
        queue_name = (
            f"{settings.REDIS_KEY_PREFIX}-"
            f"{fully_qualified_function_name(fn)}"
        )
        # Ensures that async calls are assigned to the correct actors,
        # despite the DB check wrapper
        actor_name = fully_qualified_function_name(fn)
        actor_instance = dramatiq_actor(
            fn_with_db_check,
            *args,
            **kwargs,
            queue_name=queue_name,
            max_retries=max_retries,
            actor_name=actor_name,
        )
        return ActorWrapper(actor=actor_instance)

    if fn is None:
        return decorator
    else:
        return decorator(fn)
