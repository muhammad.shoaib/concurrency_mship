import asyncio
import aiohttp


async def generate_request() -> None:
    # url = f'http://127.0.0.1:8000/plain/'
    url = f'http://127.0.0.1:8000/both/'

    # asynchronous context managers
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            # resp.raise_for_status()
            print(await resp.text())


def main():
    global loop
    loop = asyncio.get_event_loop()
    loop.run_until_complete(get_title_range())
    print("Done.")


async def get_title_range():
    tasks = []
    for n in range(0, 100):
        tasks.append((n, loop.create_task(generate_request())))

    for n, t in tasks:
        # This actually executed the task
        await t


if __name__ == '__main__':
    main()
