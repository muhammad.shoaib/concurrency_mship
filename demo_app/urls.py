from django.urls import path
from demo_app.views import both, index_django_q, index_dramatiq, plain

urlpatterns = [
    path("both/", both),
    path("plain/", plain),
    path("djangoq/", index_django_q),
    path("dramatiq/", index_dramatiq)
]