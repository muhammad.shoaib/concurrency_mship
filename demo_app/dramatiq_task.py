import logging
import uuid

from demo_app.models import Order
from mothership.async_queue.local_api import run_async
from time import sleep

# logger = logging.getLogger(__name__)


@run_async(max_retries=0)
def my_dramatiq_task() -> None:
    uuid_str = str(uuid.uuid4())
    print(uuid_str)

    # sleep(5)
    # logger.info(f"This is dramatiq")
    order = Order.objects.get(id=1)
    order.name = uuid_str
    # order.save(update_fields=["name"])
    order.save()
