from time import sleep
from demo_app.models import Order
import uuid

# Django Q
def sleep_and_print(secs):
    uuid_str = str(uuid.uuid4())
    print(uuid_str)

    #sleep(secs)
    order = Order.objects.get(id=1)
    # sleep(5)

    order.name = uuid_str
    # order.save(update_fields=["name"])
    order.save()
    print("Task ran!")
