from django.db import models
from ool import VersionField, VersionedMixin


class Order(VersionedMixin,  models.Model):
    """Represents an order for a single transport."""

    name = models.TextField(default='alpha')
    version = VersionField()
