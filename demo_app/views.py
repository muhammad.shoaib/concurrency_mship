import uuid

from django.http import JsonResponse
from django_q.tasks import async_task
from demo_app.models import Order
from django.db import transaction

from demo_app.dramatiq_task import my_dramatiq_task


# @transaction.atomic
def plain(request):
    json_payload = {"message": "hello world!"}

    uuid_str = str(uuid.uuid4())
    print(uuid_str)

    order = Order.objects.get(id=1)
    order.name = uuid_str
    # order.save(update_fields=["name"])
    order.save()

    return JsonResponse(json_payload)


def both(request):
    json_payload = {"message": "hello world!"}

    # Django Q
    async_task("demo_app.django_q_task.sleep_and_print", 0)
    # Dramatiq
    my_dramatiq_task.send()

    return JsonResponse(json_payload)


def index_django_q(request):
    json_payload = {"message": "hello world!"}
    async_task("demo_app.django_q_task.sleep_and_print", 0)
    return JsonResponse(json_payload)


def index_dramatiq(request):
    json_payload = {"message": "hello world!"}
    my_dramatiq_task.send()
    return JsonResponse(json_payload)
